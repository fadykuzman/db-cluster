#! /bin/bash

usage() {
	echo "usage: ./start_envs.sh [prod|env]"
	exit 1
}
start_prod() {
	echo "Starting Production Environment"
	docker-compose -f ./prod/docker-compose.yml up -d
}

start_dev() {
	echo "Starting Development Environment"
	docker-compose -f ./dev/docker-compose.yml up -d
}

if [ $# -ge 2 ]; then
	echo "Providing more than one argument is not supported"
	echo ""
	usage
fi

if [ -z $1 ]; then
	echo "Providing no arguments will start both the Production and the Development environment"
	echo "Are you sure you want to do that?"
	echo "y for yes"
	echo "n for no"
	echo "any other key to quit"
	read answer
elif [ $1 = 'prod' ]; then
	start_prod
	exit 0
elif [ $1 = 'dev' ]; then
	start_dev
	exit 0
fi

if [ $answer == 'y' ]; then
	start_prod
	start_dev
fi

if [ $answer == 'n' ]; then
	echo "Would you like to specify an environment?"
	echo "Right now, prod for production and dev for development are supported"
	usage
fi
