#! /bin/bash

usage() {
	echo ""
	echo "usage: ./recreate_container.sh ENVIRONMENT [Container_Name...]"
	echo "ENVIRONMENT MUST be either dev or prod"
	echo ""
}

run_docker() {
	docker-compose -f ./$environment/docker-compose.yml up -d --force-recreate --build $services
}

if [ $# -eq 0 ]; then
	usage
	exit 1
fi

echo "$1"
if [ $1 != 'dev' ]; then #|| [ $1 != 'prod' ]; then
	echo "You have to specify a supported envirnoment"
	usage
	exit 1
else
	services="${@:2}"
	if [ "$services" = "" ]; then echo "Recreating all services in cluster"
	else 
		echo "Then following services will be recreated"
		echo "$services"
	fi
	environment="$1"
	run_docker
fi
