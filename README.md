# DB Cluster using docker compose

Here I built up a cluster of the following components:
- mysql:latest
- adminer:latest
- liquibase:latest
- custom service see [simpleservice|https://gitlab.com/fadykuzman/tdd-simpleserver-springboot/]

## Port-forwarding
This is a list of the host ports where the above services container internal ports are forwarded to

|Service|Dev |Prod|
|---|---|---|
|API Service|8081|8082|
|MySQL|3307|3306|
|Adminer|8091|8092|
