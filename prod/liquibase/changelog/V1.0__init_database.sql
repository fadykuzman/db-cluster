CREATE TABLE company (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30) NOT NULL,
    	address VARCHAR(30) NOT NULL,
    	phone VARCHAR(30) NOT NULL,
    	contact_first_name VARCHAR(30) NOT NULL,
    	contact_last_name VARCHAR(30) NOT NULL,
    	contact_phone VARCHAR(30) NOT NULL
);
