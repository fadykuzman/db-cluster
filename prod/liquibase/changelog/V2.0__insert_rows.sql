INSERT INTO
  company(
    id,
    name,
    address,
    phone,
    contact_first_name,
    contact_last_name,
    contact_phone
  )
VALUES(
    1,
    'CompuMe',
    'London 1',
    '111-222-333',
    'Guy',
    'Fuchs',
    '999-000-888'
  ),
(
    2,
    'MeineFirma',
    'Berlin 89',
    '012-034-056',
    'Helmut',
    'Schmiet',
    '098-076-054'
  ),
(
    3,
    'Mi empresa',
    'Barcelona 77',
    '014-032-056',
    'Salvador',
    'Dali',
    '047-670-528'
  );

